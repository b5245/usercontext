import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from './UserContext';


export default function Navbar() {

    const email = localStorage.getItem('email')
    // const { user } = useContext(UserContext)

    return (
        <div>

            <Link to='/'>Home  </Link>

            <Link to='/about'>About  </Link>

            {(email !== null) ?
                <Link to='/logout'>Logout  </Link>
                :
                <React.Fragment>
                    <Link to='/login'>Login  </Link>
                    <Link to='/register'>Register  </Link>
                </React.Fragment>
            }




        </div >
    );
}



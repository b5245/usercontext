import React, { useState } from 'react';
import Navbar from './Navbar';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Home from './Home';
import About from './About';
import { UserProvider } from './UserContext';
import Login from './login';
import Register from './register';
import Logout from './logout';

const App = () => {

  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        <UserProvider value={{ user, setUser }}>
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/about' element={<About />} />
            <Route path='/login' element={<Login />} />
            <Route path='/register' element={<Register />} />
            <Route path='/logout' element={<Logout />} />
          </Routes>
        </UserProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;

import React, { useContext } from 'react';
import UserContext from './UserContext';

const Home = () => {

    const { user } = useContext(UserContext)

    return (
        <div>
            <h1>Home</h1>
            <p>{user.email}</p>
        </div>
    );
}

export default Home;

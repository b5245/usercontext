import React, { useContext, useEffect } from 'react';
import UserContext from './UserContext';
import { Navigate } from 'react-router-dom'

const Logout = () => {

    const { user, setUser } = useContext(UserContext)



    useEffect(() => {
        setUser({ email: null })
    })

    localStorage.clear()

    return (
        <Navigate to='/login' />
    )
}

export default Logout;

import React, { useContext, useEffect, useState } from 'react';
import UserContext from './UserContext';
import { Navigate } from 'react-router-dom'

const Login = () => {

    // -------------------------------------------------------
    const [email, setEmail] = useState('')
    const [pass, setPass] = useState('')
    const [active, setActive] = useState(true)

    const { user, setUser } = useContext(UserContext)
    // -------------------------------------------------------



    // -------------------------------------------------------
    useEffect(() => {
        if (email !== '' && pass !== '') {
            setActive(true)
        }

        else {
            setActive(false)
        }
    }, [email, pass])
    // -------------------------------------------------------




    // -------------------------------------------------------
    function login(e) {

        localStorage.setItem('email', email)

        setUser({
            email: localStorage.getItem('email')
        })
    }
    // -------------------------------------------------------


    console.log(email)
    console.log(pass)
    return (
        (user.email !== null) ?
            <Navigate to='/' />
            :


            <div>
                <form onSubmit={login}>
                    <p>email</p>
                    <input type="text" value={email} onChange={e => setEmail(e.target.value)} />
                    <p>password</p>
                    <input type="password" value={pass} onChange={e => setPass(e.target.value)} /><br />
                    {active ?
                        <button>Login</button>
                        :
                        <button disabled>Login</button>
                    }

                </form>
            </div>

    );
}

export default Login;
